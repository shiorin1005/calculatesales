package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数　args[0]実行構成から設定した先の資料
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理　ここで引数を呼び出している
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}
		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		//File(配列)型にargs[0]のファイルをlistFilesメソッドを使って取得した
		File[] files = new File(args[0]).listFiles();
		if(args.length != 1) {
			System.out.println("UNKNOWN_ERROR");
			return;
		}
		//listFilesメソッドを利用してargs内にあるファイルを呼び出している
		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
		//フォルダにあるすべてのファイルからi番目のファイルの名前だけを取得し、
		//それが正規表現にマッチするファイルだけrcdFilesに追加する。
		//isFileでファイルかどうかを確認している
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//for文の中にfor文を入れるとややこしくなるため外で行うのが基本
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int letter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((letter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//<rcdFilesでiを要素の数まで１ずつ増やす
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				//ここでrcdファイルをsalesfileに代入している。
				File salesfile = rcdFiles.get(i);

				FileReader fr = new FileReader(salesfile);
				br = new BufferedReader(fr);
				//ここでString型のrcdlinesという箱を作っている
				List<String> rcdlines = new ArrayList<>();
				String line;
				while ((line = br.readLine()) != null) {
					rcdlines.add(line);
				}
				if(!branchSales.containsKey(rcdlines.get(0))) {
					System.out.println("<該当ファイル名>の支店コードが不正です");
					return ;
				}
				if(rcdlines.size() != 2){
					System.out.println("<該当ファイル名>のフォーマットが不正です");
					return;
				}
				//rcdlines.get(0) = 支店コード "001"
				//rcdlines.get(1) = 売上金額
				//売上金額

				long fileSale = Long.parseLong(rcdlines.get(1));
				if(!rcdlines.get(1).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
				}
				//branchSales.get("支店コード");
				//branchSales.get(rcdlines.get(0));
				//rcdlines.get(0) + branchSales.get(0);→2つ目のマップに売上ファイルの対応するコード001
				//があるrcdlinesのget0を取得することで同じ値のkeyを取得したことになる。
				//そこに売上金額を足している。
				Long saleAmount = branchSales.get(rcdlines.get(0)) + fileSale;
				//足しただけでは意味がないため、putメソッドでbranchSalesにkeyを001に設定して
				//売上金額を入れている。
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(rcdlines.get(0), saleAmount);
			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			//↓でどのファイルを読み込むか指定し、file変数に入れている。
			File file = new File(path, fileName);
			//↓ファイルを読み込む前にそのファイルが存在しなかったらエラー表示して戻す処理を追加する。
			//trueなら処理を継続するため特に記載しない
			if(!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				//1行ずつ読み込んだ支店定義ファイルを，で分けている。
				String[] items = line.split(",");
				//splitメソッドを利用して．で分けているためそれをMapに入れる前に
				//フォーマットどおりになっているかを確認
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))){
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}
				branchNames.put(items[0], items[1]);
				//branchSales.put("支店コード","売上金額");
				//branchSales.put("001",0);
				branchSales.put(items[0], 0L);

				System.out.println(line);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	//↓ここで呼び出した引数が渡されて、名前をpathやfileNameに変えている。
	//最終段階のため数値も上で行った処理が反映されている。
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try { //new
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);//ここでファイルを作成している
			bw = new BufferedWriter(fw);

			String line;
			//拡張for文で支店コード、支店名を一行ずつ読み込んで、
			//keySetでvalueに支店名と売上金額を取得している。
			for (String key : branchNames.keySet()) {
				//支店コードをkeyとして支店名、売上金額を一行ずつ書いている。
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				//改行している
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}